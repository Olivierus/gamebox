﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private BalancingAtWorkManager gameManager;
    private TowerManager towerManager;
    public static GameObject currentObject;
    private Camera cam;
    private GameObject lastTowerObject;
    private bool canFollow = false;
    private float cameraFloor;

    private void Start()
    {
        towerManager = FindObjectOfType<TowerManager>();
        cam = Camera.main;
        cameraFloor = cam.transform.position.y;
    }

    private void Update()
    {
        if (!TryGetGameManager()) { return; }

        if (!gameManager.inGame) { return; }

        lastTowerObject = towerManager.GetLastAdded();

        Input();

        if (CanMoveCamera() && lastTowerObject != null)
            CameraFollow();
    }

    private void FixedUpdate()
    {
        if (!TryGetGameManager()) { return; }

        if (!gameManager.inGame) { return; }

        if (!towerManager.IsThereATower()) { return; }

        if (canFollow)
        {
            FollowMouse(currentObject.GetComponent<Rigidbody2D>());
        }
    }

    public void ResetCamara()
    {
        Vector3 camPos = cam.transform.position;
        camPos.y = cameraFloor;
        cam.transform.position = camPos;
    }

    private void Input()
    {
        if (UnityEngine.Input.GetMouseButtonDown(0))
        {
            currentObject = towerManager.AddTowerObject();
            currentObject.GetComponent<Rigidbody2D>().AddTorque(5f);
        }

        if (UnityEngine.Input.GetMouseButton(0))
        {
            canFollow = true;
        }
        else
        {
            canFollow = false;
        }
    }

    private bool CanMoveCamera()
    {
        bool canMove = false;

        bool checkTop = UnityEngine.Input.mousePosition.y > Screen.height - Screen.height / 3;
        bool checkBottom = UnityEngine.Input.mousePosition.y < 0 + Screen.height / 3;

        if (checkTop || checkBottom || !canFollow)
            canMove = true;

        return canMove;
    }

    private void CameraFollow()
    {
        float smoothSpeed = 0.05f;
        if (towerManager.highest == null) { return; }

        GameObject highestObject = towerManager.highest;

        Vector3 offset = new Vector3(0, 1, 0);

        Vector3 smoothedPosition = Vector3.Lerp(cam.transform.position, highestObject.transform.position + offset, smoothSpeed);

        float smoothedY = Mathf.Clamp(smoothedPosition.y, cameraFloor, highestObject.transform.position.y + offset.y);
        smoothedPosition = new Vector3(cam.transform.position.x,
                                        smoothedY,
                                        cam.transform.position.z);

        cam.transform.position = smoothedPosition;
    }

    private void FollowMouse(Rigidbody2D objectRigidbody)
    {
        Vector3 mouseToWorldPosition = Utilities.GetMouseWorldPosition();
        objectRigidbody.MovePosition(mouseToWorldPosition);
    }

    private bool TryGetGameManager()
    {
        if (gameManager != null) { return true; }

        try
        { gameManager = FindObjectOfType<BalancingAtWorkManager>(); }
        catch
        {
            Debug.LogWarning("No gameManager yet");
        }
        return false;
    }
}
