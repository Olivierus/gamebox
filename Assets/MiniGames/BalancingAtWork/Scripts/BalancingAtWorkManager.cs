﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class BalancingAtWorkManager : MonoBehaviour, IMiniGameManager
{
    public bool inGame { get; set; }
    public float score { get; set; }
    public string unit { get { return "Meters"; } }

    private Camera cam;
    private Text heigthText;
    private Timer gameCountDown;
    private GameObject currentObject;
    private TowerManager towerManger;

    private void Update()
    {
        if (!inGame) { return; }

        heigthText.text = towerManger.GetHeight().ToString() + "m";
    }

    public void StartGame()
    {
        inGame = true;
        towerManger = FindObjectOfType<TowerManager>();
        cam = Camera.main;
        SetupGround();
        heigthText = GameObject.Find("Height").GetComponent<Text>();
        gameCountDown = FindObjectOfType<Timer>();
        gameCountDown.StartTimer(true, GameOverState);
    }

    public void GameOver()
    {
        inGame = false;
        gameCountDown.StopTimer();
        score = towerManger.GetHeight();
    }

    public void RestartGame()
    {
        towerManger.DestroyAllTowerObject();
        FindObjectOfType<PlayerMovement>().ResetCamara();
    }

    private void SetupGround()
    {
        GameObject ground = GameObject.Find("Ground");

        float x = Screen.width / 2;
        Vector3 position = new Vector3(x, 0f, 0f);

        Vector3 groundScreenToWorld = cam.ScreenToWorldPoint(position);
        groundScreenToWorld.z = 0f;
        groundScreenToWorld.y += ground.GetComponent<Collider2D>().bounds.size.y / 2;
        ground.transform.position = groundScreenToWorld;
    }

    private void GameOverState()
    {
        FSM.Instance.InvokeState(States.GameOver);
    }
}
