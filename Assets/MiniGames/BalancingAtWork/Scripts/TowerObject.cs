﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerObject : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject towerParent = MakeParentObject();

        if (collision.transform.parent != null || collision.gameObject == towerParent)
        {
            transform.SetParent(towerParent.transform);
        }
        else if (collision.gameObject.name == "Ground")
        {
            transform.SetParent(towerParent.transform);
        }
    }

    private static GameObject MakeParentObject()
    {
        GameObject towerParent = GameObject.Find("Tower");
        if (towerParent == null) { towerParent = new GameObject("Tower"); }

        return towerParent;
    }
}
