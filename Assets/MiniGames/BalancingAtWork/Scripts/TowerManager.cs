﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TowerManager : MonoBehaviour
{
    [HideInInspector]
    public GameObject highest;

    private GameObject[] towerObjects;
    private List<GameObject> tower = new List<GameObject>();

    private void Start()
    {
        towerObjects = Resources.LoadAll("BalancingAtWork/Objects", typeof(GameObject)).Cast<GameObject>().ToArray();
    }

    private void Update()
    {
        if (!IsThereATower()) { return; }

        GetHighest();
        print(highest.name);

        DestoryOutOfPlayField();
    }

    public bool IsThereATower()
    {
        if (tower.Count == 0)
            return false;
        else
            return true;
    }

    public GameObject AddTowerObject()
    {
        GameObject towerObject = Create();
        tower.Add(towerObject);
        return towerObject;
    }

    public GameObject GetLastAdded()
    {
        if (!IsThereATower()) { return null; }
        return tower.Last();
    }

    public int GetHeight()
    {
        if (!IsThereATower()) { return 0; }

        if (highest == default) { return 0; }

        float lastObjectYPosition = highest.transform.position.y;
        int HeightInInt = Mathf.RoundToInt(lastObjectYPosition);
        return 1 + HeightInInt;
    }

    private GameObject Create()
    {
        int towerObjectIndex = Random.Range(0, towerObjects.Length);
        Vector3 mouseToWorldPosition = Utilities.GetMouseWorldPosition();
        GameObject towerObject = Instantiate(towerObjects[towerObjectIndex], mouseToWorldPosition, Quaternion.identity);
        towerObject.name = towerObject.name.Replace("(Clone)", "");
        towerObject.AddComponent<TowerObject>();
        return towerObject;
    }

    private void GetHighest()
    {
        int indexHighest = 0;
        float highestYValue = 0;

        for (int i = 0; i < tower.Count; i++)
        {
            if (tower[i].transform.parent == null) { continue; }

            if (tower[i] == PlayerMovement.currentObject) { continue; }

            float currentObjectHeight = Mathf.Abs(tower[i].transform.position.y);
            if (currentObjectHeight > highestYValue)
            {
                indexHighest = i;
                highestYValue = currentObjectHeight;
            }
        }
        highest = tower[indexHighest];
    }

    private void DestoryOutOfPlayField()
    {
        for (int i = 0; i < tower.Count; i++)
        {
            GameObject currentTowerObject = tower[i];
            if (currentTowerObject.transform.position.y < -3f)
            {
                DestroyTowerObject(currentTowerObject);
            }
        }
    }

    private void DestroyTowerObject(GameObject currentTowerObject)
    {
        tower.Remove(currentTowerObject);
        Destroy(currentTowerObject);
    }

    public void DestroyAllTowerObject()
    {
        int count = tower.Count;
        for (int i = 0; i < count; i++)
        {
            DestroyTowerObject(tower.First());
        }
    }
}
