﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;

public class SceneGenerator : MonoBehaviour
{
    [HideInInspector]
    public SceneGeneratorData data;
    public ObstacleData obstacleData;
    private List<GameObject> currentObstacles = new List<GameObject>();
    private GameObject[] allObstacleObjects;
    private Sprite[] obstacleSprites;

    private void Awake()
    {
        allObstacleObjects = Resources.LoadAll("Washingtime/Obstacles", typeof(GameObject)).Cast<GameObject>().ToArray();
        obstacleSprites = Resources.LoadAll("Washingtime/Obstacles/Textures", typeof(Sprite)).Cast<Sprite>().ToArray();
    }

    private void Start()
    {
        CreateObstacles();
    }

    public void CreateObstacles()
    {
        for (int i = 0; i < data.amountOfObstacles; i++)
        {
            currentObstacles.Add(CreateObstacle());
        }
    }

    public void AddObstacle()
    {
        currentObstacles.Add(CreateObstacle());
    }

    private GameObject CreateObstacle()
    {
        GameObject obstacleToSpawn = allObstacleObjects[Random.Range(0, allObstacleObjects.Length)];

        GameObject obstacle = Instantiate(obstacleToSpawn, Utilities.RandomVector3(-10f, 10f), Quaternion.identity);
        obstacle.name = obstacle.name.Replace("(Clone)", "");

        SpriteRenderer obstacleSpriteRenderer = obstacle.GetComponent<SpriteRenderer>();
        obstacleSpriteRenderer.sprite = obstacleSprites[Random.Range(0, obstacleSprites.Length)];

        PolygonCollider2D col = obstacle.AddComponent<PolygonCollider2D>();
        col.isTrigger = true;

        Obstacle currentObstacle = obstacle.GetComponent<Obstacle>();
        currentObstacle.data = this.obstacleData;

        return obstacle;
    }

    public void DestroyObstacles()
    {
        foreach (GameObject obstacle in currentObstacles)
        {
            Destroy(obstacle);
        }
    }
}

[System.Serializable]
public class SceneGeneratorData
{
    public int amountOfObstacles = 3;
}