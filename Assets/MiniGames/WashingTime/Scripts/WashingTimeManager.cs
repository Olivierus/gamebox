using System;
using UnityEngine;

public class WashingTimeManager : MonoBehaviour, IMiniGameManager
{
    public bool inGame { get; set; }
    public float score { get; set; }
    public string unit { get { return "Seconds"; } }

    public PlayerControllerData playerData = new PlayerControllerData();
    public SceneGeneratorData sceneData = new SceneGeneratorData();
    public Edge2CircleData edge2CircleData = new Edge2CircleData();
    public ObstacleData obstacleData = new ObstacleData();

    private PlayerController playerController;
    private SceneGenerator sceneGenerator;
    private Edge2CircleCollider edge2CircleCollider;

    private GameObject playerObject;
    private GameObject washingMachineRaduis;

    private void Awake()
    {
        // RestartGame();
        UIManager.Instance.ShowUIPanel(UI.UI_WashingTime, true);
    }

    public void StartGame()
    {
        inGame = true;
        // FMODAudioManager.WashingTimeUIStartGame.oneShot();
        CreatePlayer();
        StartCountDown(CreateObstacles);
    }

    public void GameOver()
    {
        FindObjectOfType<GameTimer>().gameStarted = false;
        score = FindObjectOfType<GameTimer>().time;
        playerController.FallApart();
        playerController.enabled = false;
        score = FindObjectOfType<GameTimer>().time;
    }

    public void RestartGame()
    {
        if (inGame)
        {
            Destroy(playerObject);
            sceneGenerator.DestroyObstacles();
            Destroy(sceneGenerator);
            Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Obstacles"), false);
            FindObjectOfType<RotateObject>().mayRotate = false;

            inGame = false;
        }
        else
        {
            CreateLevel();
        }
    }

    private void StartCountDown(Action createObstacles)
    {
        FindObjectOfType<GameTimer>().CountDown(createObstacles);
        FindObjectOfType<RotateObject>().mayRotate = true;
    }

    private void CreateObstacles()
    {
        sceneGenerator = gameObject.AddComponent<SceneGenerator>();
        sceneGenerator.data = this.sceneData;
        sceneGenerator.obstacleData = this.obstacleData;
    }

    private void CreatePlayer()
    {
        playerObject = Instantiate(Resources.Load<GameObject>("Washingtime/Player"));
        playerObject.name = playerObject.name.Replace("(Clone)", "");
        playerController = playerObject.GetComponent<PlayerController>();
        playerController.data = this.playerData;
    }

    private void CreateLevel()
    {
        edge2CircleCollider = FindObjectOfType<Edge2CircleCollider>();
        edge2CircleCollider.data = this.edge2CircleData;
    }
}
