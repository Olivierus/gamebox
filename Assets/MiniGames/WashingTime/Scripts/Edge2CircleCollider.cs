﻿using UnityEngine;

[ExecuteInEditMode]
public class Edge2CircleCollider : MonoBehaviour
{
    [HideInInspector]
    public Edge2CircleData data;

    void Start()
    {
        EdgeCollider2D edgeCollider = GetComponent<EdgeCollider2D>();

        Vector2[] points = Utilities.CreateCircle(data.numberOfEdges, data.raduis);

        edgeCollider.points = points;
    }


}

[System.Serializable]
public class Edge2CircleData
{
    public int numberOfEdges = 64;
    public float raduis = 4f;
}
