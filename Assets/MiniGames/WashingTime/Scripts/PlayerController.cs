﻿using UnityEngine;

public class PlayerController : MonoBehaviour, IPlayerController
{
    [HideInInspector]
    public PlayerControllerData data { get; set; }
    public bool grounded { get; set; }
    private Joint2D[] characterJoints;
    private GameObject[] characterParts;
    private Rigidbody2D rb;
    private int amountOfJumps;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        characterJoints = GetComponentsInChildren<Joint2D>();

        characterParts = new GameObject[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            GameObject go = characterParts[i];
            go = transform.GetChild(0).GetChild(i).gameObject;
        }
    }

    private void FixedUpdate()
    {
        Move();

        if (grounded)
            Jump();

        CheckGrounded();
    }

    public void Jump()
    {
        if (amountOfJumps > 1)
        {
            grounded = false;
            return;
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            amountOfJumps += 1;

            rb.velocity = Vector3.zero;
            rb.velocity += Vector2.up * data.jumpPower;
        }

        if (rb.velocity.y < 0)
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (data.fallSpeed - 1) * Time.deltaTime;
        }
        else if (rb.velocity.y > 0 && !Input.GetKey(KeyCode.W))
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (data.ascentSpeed - 1) * Time.deltaTime;
        }
    }

    public void Move()
    {
        if (Input.GetKey(KeyCode.A))
        {
            rb.velocity += Vector2.left * data.moveSpeed;
        }
        if (Input.GetKey(KeyCode.D))
        {
            rb.velocity += Vector2.right * data.moveSpeed;
        }
    }

    public void FallApart()
    {
        for (int i = 0; i < characterJoints.Length; i++)
        {
            Destroy(characterJoints[i].GetComponent<OnHit>());
            Destroy(characterJoints[i]);
        }
    }

    private void CheckGrounded()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, 1.75f, 1 << 8);

        if (hit == default) { return; }

        if (hit.collider.gameObject.name.Contains("Radius"))
        {
            grounded = true;
            amountOfJumps = 0;
        }
    }
}

[System.Serializable]
public class PlayerControllerData
{
    public float jumpPower = 30f;
    public float ascentSpeed = 5f;
    public float fallSpeed = 2.5f;
    public float moveSpeed = 1f;
}
