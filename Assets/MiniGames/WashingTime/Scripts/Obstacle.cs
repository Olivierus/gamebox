﻿using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public ObstacleData data;

    private Vector2[] path;
    private Rigidbody2D rb;
    private int pathIndex = 0;
    private float maxRadius;

    private void Awake()
    {
        path = new Vector2[data.pathResolution];
        maxRadius = FindObjectOfType<Edge2CircleCollider>().data.raduis;
        rb = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        CreateObstaclePath();
        SetStartPoint();
    }

    private void SetStartPoint()
    {
        int pathRange = path.Length / 3;
        int min = 0;
        int max = path.Length - (pathRange * 2);

        pathIndex = Random.Range(min, max);
        transform.position = path[pathIndex];
    }

    private void FixedUpdate()
    {
        Revolve();
        VisualizePath();
    }

    private void VisualizePath()
    {
        for (int i = 0; i < path.Length; i++)
        {
            try
            {
                Debug.DrawLine(path[i], path[i + 1]);
            }
            catch
            {
                return;
            }
        }
    }

    private void Revolve()
    {
        if (Mathf.Approximately(Vector2.Distance(transform.position, path[pathIndex]), 0f)) { pathIndex++; }

        if (pathIndex == path.Length) { pathIndex = 0; }

        rb.velocity = Vector2.zero;

        float maxDistanceDelta = Random.Range(data.revoleTime.x, data.revoleTime.y);
        Vector3 nextStep = Vector3.MoveTowards(transform.position, path[pathIndex], maxDistanceDelta);
        rb.MovePosition(nextStep);
    }

    private void CreateObstaclePath()
    {
        float radius = Random.Range(maxRadius, maxRadius * data.offsetStrength);
        path = Utilities.CreateCircle(data.pathResolution, radius);
    }
}

[System.Serializable]
public class ObstacleData
{
    public float offsetStrength = 0.9f;
    public int pathResolution = 10;
    public Vector2 revoleTime = new Vector2(0.1f, 0.3f);
}