﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateObject : MonoBehaviour
{
    public bool mayRotate = false;

    public float RotationSpeed = 25f;

    void Update()
    {
        if (mayRotate)
            transform.Rotate(Vector3.forward * (RotationSpeed * Time.deltaTime));
    }
}
