﻿using UnityEngine;

public class OnHit : MonoBehaviour
{
    public delegate void partHit();
    [HideInInspector]
    public partHit BodyPartHit;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name.Contains("Obstacle"))
        {
            Physics2D.IgnoreLayerCollision(gameObject.layer, collision.gameObject.layer, true);
            FSM.Instance.SwitchStates(FSM.Instance.StatesEnumToState(States.GameOver));
        }
    }
}
