﻿using UnityEngine;

public class GeneratorComponent : MonoBehaviour
{
    public float speed = 0;
    private Camera mainCamera;

    private void Awake()
    {
        mainCamera = Camera.main;
    }

    private void Update()
    {
        if (speed != 0f)
            MoveComponent(speed);

        IsComponentOutOfView();
    }

    public void MoveComponent(float componentMoveSpeed)
    {
        speed = componentMoveSpeed;
        transform.position += Vector3.left * (speed * Time.deltaTime);
    }

    public bool IsComponentOutOfView()
    {
        Vector3 worldPositionComponent = transform.position + GetComponent<Renderer>().bounds.size;
        Vector3 screenPositionComponent = mainCamera.WorldToScreenPoint(worldPositionComponent);

        if (screenPositionComponent.x < 0)
        {
            return true;
        }
        return false;
    }
}
