﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RowGenerator : Generator
{
    public override void Tick()
    {
        base.Tick();
        if (!FirstInRowOutOfView()) { return; }
        MoveToBack(NextComponentPosition());
    }
}
