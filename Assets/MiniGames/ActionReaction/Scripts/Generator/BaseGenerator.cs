﻿using System.Linq;

public class BaseGenerator : Generator
{
    public override void Tick()
    {
        base.Tick();
        if (FirstInRowOutOfView())
        {
            RemoveComponent(allComponents.First().gameObject);
        }
    }
}
