﻿using UnityEngine;

public interface IGenerator
{
    GameObject nextComponentInRow { get; set; }
    float componentSpeed { get; set; }
    GameObject[] obstacles { get; set; }

    void InnitComponents(Vector3 startPosition);
    void Tick();
    void AddComponent(Vector3 position);
    void ResetComponents();
}
