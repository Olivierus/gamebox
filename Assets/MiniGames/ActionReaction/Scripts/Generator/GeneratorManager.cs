﻿using System;
using System.Linq;
using UnityEngine;

public class GeneratorManager : MonoBehaviour
{
    private IGenerator backgroundVisuals;
    private IGenerator playFieldGenerator;
    private IGenerator blindsGenerator;
    public IGenerator truckGenerator;

    public float baseSpeed;

    public void CreateAllGenerators()
    {
        backgroundVisuals = gameObject.AddComponent<RowGenerator>();
        backgroundVisuals.nextComponentInRow = (GameObject)Resources.Load("ReactionTime/Background");
        backgroundVisuals.componentSpeed = baseSpeed * 1f;
        backgroundVisuals.InnitComponents(SetPosition(Screen.height));

        GameObject[] obstacles = Resources.LoadAll("ReactionTime/Obstacles", typeof(GameObject)).Cast<GameObject>().ToArray();
        playFieldGenerator = gameObject.AddComponent<PlayFieldGenerator>();
        playFieldGenerator.nextComponentInRow = (GameObject)Resources.Load("ReactionTime/Ground");
        playFieldGenerator.componentSpeed = baseSpeed * 2f;
        playFieldGenerator.obstacles = obstacles;
        playFieldGenerator.InnitComponents(SetPosition(0));

        truckGenerator = gameObject.AddComponent<BaseGenerator>();
        truckGenerator.nextComponentInRow = (GameObject)Resources.Load("ReactionTime/Truck");
        truckGenerator.componentSpeed = baseSpeed * 2f;

        GameObject blindParent = new GameObject("Blind");
        blindsGenerator = blindParent.AddComponent<RowGenerator>();
        blindsGenerator.nextComponentInRow = (GameObject)Resources.Load("ReactionTime/Blinds");
        blindsGenerator.componentSpeed = baseSpeed * 3f;
        blindsGenerator.InnitComponents(SetPosition(0));
    }

    private Vector3 SetPosition(float yOffset)
    {
        Vector3 position = Camera.main.ScreenToWorldPoint(new Vector3(0, yOffset, 0f));
        position.z = 0;
        return position;
    }

    public void UpdateSpeed(float addToSpeed)
    {
        baseSpeed = addToSpeed;
        backgroundVisuals.componentSpeed = baseSpeed * 1;
        playFieldGenerator.componentSpeed = baseSpeed * 2;
        truckGenerator.componentSpeed = baseSpeed * 2;
        blindsGenerator.componentSpeed = baseSpeed * 3;
    }

    public void Tick()
    {
        backgroundVisuals.Tick();
        playFieldGenerator.Tick();
        blindsGenerator.Tick();
        truckGenerator.Tick();
    }

    public void ResetAllGenerators()
    {
        backgroundVisuals.ResetComponents();
        playFieldGenerator.ResetComponents();
        blindsGenerator.ResetComponents();
    }
}
