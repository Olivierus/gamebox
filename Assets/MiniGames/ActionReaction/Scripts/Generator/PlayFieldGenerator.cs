﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PlayFieldGenerator : Generator
{
    private int componentsBetweenObstaclesChance = 0;
    private Vector3 defaultPosition = new Vector3(0, 10, 0);

    public List<GeneratorComponent> allObstacles = new List<GeneratorComponent>();
    private List<GeneratorComponent> obstacleObject = new List<GeneratorComponent>();

    public override void Tick()
    {
        if (FirstInRowOutOfView())
        {
            FilterComponent();
            CreatePlayFieldComponent();
        }
        base.Tick();
    }

    private void FilterComponent()
    {
        if (allComponents.First().gameObject.name.Contains("Ground"))
            MoveToBack(NextComponentPosition());
        else
            MoveObstacleBack();
    }

    private void MoveObstacleBack()
    {
        GeneratorComponent component = allObstacles.First();
        component.transform.position = defaultPosition;
        allObstacles.Remove(component);
        allComponents.Remove(component);
        obstacleObject.Add(component);
    }

    public override void InnitComponents(Vector3 startPosition)
    {
        for (int i = 0; i < 10; i++)
        {
            GameObject t = Instantiate(obstacles[Random.Range(0, obstacles.Length)], defaultPosition, Quaternion.identity);
            obstacleObject.Add(t.AddComponent<GeneratorComponent>());
        }
        base.InnitComponents(startPosition);
    }

    private void CreatePlayFieldComponent()
    {
        GameObject randomComponentNextInRow = nextComponentInRow;

        if (componentsBetweenObstaclesChance <= 0)
        {
            AddObstacle(randomComponentNextInRow);
            componentsBetweenObstaclesChance = Mathf.RoundToInt(Random.Range(3, 10));
        }
        else
        {
            componentsBetweenObstaclesChance--;
        }
    }

    public bool CheckDistanceToObstacle(float rangeUntilObstacle)
    {
        if (allObstacles.Count < 1) { return false; }

        float distance = transform.position.x + allObstacles[GetNextObstacle()].transform.position.x;

        if (distance <= rangeUntilObstacle && distance > 0) { return true; }

        return false;
    }

    private int GetNextObstacle()
    {
        int index = 0;
        for (int i = 0; i < allObstacles.Count; i++)
        {
            if (allObstacles[i].transform.position.x > 0)
            {
                index = i;
                break;
            }
        }

        return index;
    }

    private void AddObstacle(GameObject obstacle)
    {
        GeneratorComponent component = obstacleObject.First();
        component.transform.position = NextComponentPosition();
        allObstacles.Add(component);
        allComponents.Add(component);
        obstacleObject.Remove(component);
    }

    public override void ResetComponents()
    {
        int count = allObstacles.Count;
        for (int i = 0; i < count; i++)
        {
            RemoveComponent(allObstacles.First().gameObject);
        }

        count = obstacleObject.Count;
        for (int i = 0; i < count; i++)
        {
            RemoveComponent(obstacleObject.First().gameObject);
        }

        obstacleObject.Clear();
        allObstacles.Clear();
        base.ResetComponents();
    }
}
