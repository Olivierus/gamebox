﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public abstract class Generator : MonoBehaviour, IGenerator
{
    public float componentSpeed { get; set; }
    public GameObject nextComponentInRow { get; set; }
    public GameObject[] obstacles { get; set; }

    protected Vector3 componentNextPosition;
    protected int startAmountOfComponents = 5;
    protected List<GeneratorComponent> allComponents = new List<GeneratorComponent>();

    private Vector3 basePosition;

    public virtual void Tick()
    {
        if (allComponents.Count < 1) { return; };

        foreach (GeneratorComponent component in allComponents)
        {
            component.MoveComponent(componentSpeed);
        }
    }

    public virtual void InnitComponents(Vector3 startPosition)
    {
        componentNextPosition = basePosition = startPosition;
        do { AddComponent(NextComponentPosition()); }
        while (!CheckIfComponentOutOfView(allComponents[allComponents.Count - 1].gameObject));
    }

    public void AddComponent(Vector3 position)
    {
        allComponents.Add(CreateComponent(nextComponentInRow, position));
    }

    protected GeneratorComponent CreateComponent(GameObject componentToAddToRow, Vector3 position)
    {
        GameObject component = Instantiate(componentToAddToRow, position, Quaternion.identity);
        component.AddComponent<GeneratorComponent>();
        component.transform.SetParent(transform);
        component.name = component.name.Remove(component.name.Length - 7);
        return component.GetComponent<GeneratorComponent>();
    }

    protected void RemoveComponent(GameObject component)
    {
        allComponents.Remove(component.GetComponent<GeneratorComponent>());
        Destroy(component);
    }

    protected bool FirstInRowOutOfView()
    {
        if (allComponents.Count < 1) { return false; }

        GeneratorComponent firstComponentInRow = allComponents.First();

        if (!firstComponentInRow.IsComponentOutOfView()) { return false; }

        return true;
    }

    public virtual void ResetComponents()
    {
        int count = allComponents.Count;
        for (int i = 0; i < count; i++)
        {
            RemoveComponent(allComponents.First().gameObject);
        }
        allComponents.Clear();
        InnitComponents(basePosition);
    }

    protected Vector3 NextComponentPosition()
    {
        if (allComponents.Count > 0)
        {
            GeneratorComponent component = allComponents[allComponents.Count - 1];
            componentNextPosition = component.transform.position;
            componentNextPosition.x += component.GetComponent<SpriteRenderer>().bounds.size.x;
            componentNextPosition.z = 0f;
        }

        return componentNextPosition;
    }

    private bool CheckIfComponentOutOfView(GameObject component)
    {
        Vector3 worldPositionComponent = component.transform.position - component.GetComponent<SpriteRenderer>().bounds.size;
        Vector3 screenPositionComponent = Camera.main.WorldToScreenPoint(worldPositionComponent);

        if (screenPositionComponent.x > Screen.width)
        {
            return true;
        }
        return false;
    }

    protected void MoveToBack(Vector3 position)
    {
        GeneratorComponent component = allComponents.First();
        allComponents.Remove(component);
        component.transform.position = position;
        allComponents.Add(component);
    }
}
