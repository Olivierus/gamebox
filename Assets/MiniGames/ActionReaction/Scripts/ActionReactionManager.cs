﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionReactionManager : MonoBehaviour, IMiniGameManager
{
    public bool inGame { get; set; }
    public float score { get; set; }
    public string unit { get { return "Seconds"; } }

    private BarrelController playerController;
    private GameObject playerObject;
    private List<GeneratorComponent> trucks = new List<GeneratorComponent>();
    private GeneratorManager allGenerators;
    private Timer timer;
    private Vector2 timingShow = new Vector2(2f, 6f);
    private GameObject blind;
    private float baseSpeed = 1;
    private bool canAddSpeed = true;

    public bool isShowingRandom = false;
    public bool isShowing;

    private static ActionReactionManager _instance;

    public static ActionReactionManager Instance
    {
        get
        {
            if (_instance == null)
            {
                new GameObject("scene generator", typeof(ActionReactionManager));
            }
            return _instance;
        }
    }

    private void Awake()
    {
        _instance = this;

        allGenerators = gameObject.AddComponent<GeneratorManager>();
        allGenerators.CreateAllGenerators();

        timer = FindObjectOfType<Timer>();
        blind = GameObject.Find("Blind");
        ShowBlind(false);
    }

    private void Update()
    {
        if (!inGame) { return; }

        playerController.Tick();
        playerController.rotationSpeed = baseSpeed;

        score = timer.GetTime();

        allGenerators.UpdateSpeed(IncrementSpeed());
        allGenerators.Tick();

        if (!isShowingRandom && playerController.grounded)
        {
            StartCoroutine(RandomShow(timingShow.x, timingShow.y));
        }
    }

    private float IncrementSpeed()
    {
        int s = Mathf.RoundToInt(score % 10);
        if (s == 0 && canAddSpeed == true)
        {
            baseSpeed += .1f;
            canAddSpeed = false;
        }
        if (s != 0)
        {
            canAddSpeed = true;
        }
        return baseSpeed;
    }

    public void StartGame()
    {
        playerObject = Instantiate(Resources.Load<GameObject>("ReactionTime/Player"));
        playerObject.name = playerObject.name.Replace("(Clone)", "");
        playerController = playerObject.GetComponent<BarrelController>();
        allGenerators.baseSpeed = baseSpeed = 1f;
        timer.StartTimer();
        ShowBlind(false);
        inGame = !inGame;
    }

    public void RestartGame()
    {
        allGenerators.ResetAllGenerators();
        timer.ResetTimer();
        ShowBlind(false);
    }

    public void GameOver()
    {
        inGame = !inGame;
        timer.StopTimer();
        score = timer.GetTime();
        allGenerators.UpdateSpeed(0);
        allGenerators.Tick();
        Destroy(playerObject);
        ShowBlind(false);
    }

    private IEnumerator RandomShow(float min, float max)
    {
        isShowingRandom = true;
        yield return new WaitForSecondsRealtime(UnityEngine.Random.Range(min, max));
        if (isShowing)
        {
            ShowBlind(false);
            SpawnTruck();
            yield return new WaitForSecondsRealtime(2f);
            ShowBlind(true);
        }
        isShowingRandom = false;
        yield return null;
    }

    private void SpawnTruck()
    {
        GameObject ground = GameObject.Find("Ground");
        Vector3 groundPlane = ground.GetComponent<Renderer>().bounds.size + ground.transform.position;
        Vector3 truckPosition = new Vector3(playerObject.transform.position.x + (playerObject.GetComponent<Renderer>().bounds.size.x * 3), groundPlane.y, 0f);
        allGenerators.truckGenerator.AddComponent(truckPosition);
    }

    public void ShowBlind(bool show)
    {
        blind.SetActive(show);
        if (playerController)
            playerController.canJump = show;
        isShowing = show;
    }
}

