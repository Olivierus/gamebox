﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BarrelController : MonoBehaviour, IPlayerController
{
    public PlayerControllerData data { get; set; }
    public bool grounded { get; set; }
    public bool canJump = false;
    public float rotationSpeed = 1;

    private const float rangeTillObstacle = 6f;
    private float jumpPower = 8;
    private PlayFieldGenerator playerFieldGenerator;
    private Rigidbody2D playerRigidbody;
    private GameObject deathEffect;
    public float fallSpeed;
    public float ascentSpeed;

    private void Start()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
        playerFieldGenerator = FindObjectOfType<PlayFieldGenerator>();
        deathEffect = (GameObject)Resources.Load<GameObject>("ReactionTime/deathEffect");
    }

    public void Tick()
    {
        GetDistanceObstacle();
        Jump();
        Roll();
    }

    private void Roll()
    {
        transform.Rotate(0, 0, -2 * rotationSpeed);
    }

    private void GetDistanceObstacle()
    {
        if (playerFieldGenerator == null) { return; }

        if (playerFieldGenerator.CheckDistanceToObstacle(rangeTillObstacle)) { ShowBlinds(false); }
    }

    public void Jump()
    {
        if (!ActionReactionManager.Instance.isShowing && playerRigidbody != default)
        {
            if (Input.GetKeyDown(KeyCode.Space) && grounded)
            {
                playerRigidbody.velocity += Vector2.up * jumpPower;
                grounded = false;
            }

            if (playerRigidbody.velocity.y < 0)
            {
                playerRigidbody.velocity += Vector2.up * Physics2D.gravity.y * (fallSpeed - 1) * Time.deltaTime;
            }
            else if (playerRigidbody.velocity.y > 0 && !Input.GetKey(KeyCode.W))
            {
                playerRigidbody.velocity -= Vector2.up * Physics2D.gravity.y * (ascentSpeed - 1) * Time.deltaTime;
            }
        }
    }

    public void Move() { }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name.Contains("Ground") && !grounded)
        {
            grounded = true;
            ShowBlinds(true);
        }

        if (collision.gameObject.name.Contains("Death"))
        {
            GameObject currentDeathEffect = Instantiate(deathEffect, transform.position, Quaternion.identity);
            currentDeathEffect.GetComponent<ParticleSystem>().Play();
            Destroy(currentDeathEffect, 3f);
            FSM.Instance.SwitchStates(FSM.Instance.StatesEnumToState(States.GameOver));
        }
    }

    private void ShowBlinds(bool show)
    {
        ActionReactionManager.Instance.ShowBlind(show);
        canJump = !show;
    }
}

