﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighRiskManager : MonoBehaviour, IMiniGameManager
{
    public bool inGame { get; set; }
    public float score { get; set; }
    public string unit { get { return "Mililiters"; } }

    public GameObject[] drinks;
    public Color[] fluidColors;
    public DrunkMeter drunkMeterObject;
    public int amountOfDrinks = 3;

    private List<GameObject> currentDrinks = new List<GameObject>();
    private int amountOfFluid;
    private GameObject peeBrakeObject;
    private GameObject meters;

    private void Start()
    {
        PlayerPrefs.SetString("currentGame", "HighRisk");
        FSM.Instance.InvokeState(States.GameMenu);
        meters = GameObject.Find("Meters");
        meters.SetActive(false);
        peeBrakeObject = GameObject.Find("peeBrake");
        peeBrakeObject.SetActive(false);
    }

    private void Update()
    {
        if (!inGame) { return; }

        IsDrunk();
    }

    private void IsDrunk()
    {
        if (drunkMeterObject.isFull)
        {
            FSM.Instance.InvokeState(States.GameOver);
        }
    }

    public void StartGame()
    {
        inGame = true;
        meters.SetActive(true);

        Vector3 parts = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0f, 0f));
        parts.y = parts.z = 0f;

        Vector3 nextPosition = -parts;
        parts.x = (parts.x * 2) / amountOfDrinks;

        for (int i = 0; i < amountOfDrinks; i++)
        {
            nextPosition += parts - (parts / 2f);
            CreateNewDrink(nextPosition);
            nextPosition += (parts / 2f);
        }
    }

    public void RestartGame()
    {
        for (int i = 0; i < amountOfDrinks; i++)
        {
            GameObject drinkToRemove = currentDrinks[i];
            Destroy(drinkToRemove);
        }
        currentDrinks.Clear();
        drunkMeterObject.Reset();
        meters.SetActive(true);
        SetDrinksMeter(0);
        amountOfFluid = 0;
    }

    public void GameOver()
    {
        score = amountOfFluid;
        meters.SetActive(false);
        inGame = false;
    }

    public void DrinkDrunk(GameObject drunkDrink)
    {
        drinkData drinkValues = drunkDrink.GetComponent<Drink>().GetDrinkData();

        SetDrinksMeter(drinkValues.amountOfFluid);

        SetDrunkMeter(drinkValues);

        CreateNewDrink(drunkDrink.transform.position);

        RemoveDrink(drunkDrink);
    }

    private void RemoveDrink(GameObject gameObject)
    {
        currentDrinks.Remove(gameObject);
        Destroy(gameObject);
    }

    private void CreateNewDrink(Vector3 position)
    {
        int drinksIndex = Random.Range(0, drinks.Length);
        GameObject drink = Instantiate(drinks[drinksIndex], position, Quaternion.identity);
        drink.name = drink.name.Replace("(Clone)", "");
        currentDrinks.Add(drink);
        currentDrinks[currentDrinks.Count - 1].GetComponent<Drink>().SetupDrink(fluidColors);
    }

    private void SetDrunkMeter(drinkData drinkValues)
    {
        if (drinkValues.alcahol != 0)
        {
            float amountOfDrunkToAdd = Mathf.Abs(drinkValues.amountOfFluid * (drinkValues.alcahol / 100f));
            drunkMeterObject.ChangeMeter(amountOfDrunkToAdd);
        }
        else
        {
            drunkMeterObject.LessDrunk();
            StartCoroutine("PeeBrake");
        }

    }

    public IEnumerator PeeBrake()
    {
        peeBrakeObject.SetActive(true);
        yield return new WaitForSeconds(2f);
        peeBrakeObject.SetActive(false);
    }

    private void SetDrinksMeter(int amountOfFluid)
    {
        TextMesh drinksText = GameObject.Find("DrinkMeter").GetComponent<TextMesh>();
        this.amountOfFluid += Mathf.RoundToInt(amountOfFluid);
        drinksText.text = this.amountOfFluid.ToString() + " ml";
    }
}
