﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrunkMeter : MonoBehaviour
{
    public GameObject meterObject;
    public GameObject textObject;
    public GameObject maxObject;

    public Color baseColor;
    public Color drunkColor;

    public bool isFull { get; private set; }

    private float meter;
    private float maxLength;

    private void Start()
    {
        maxLength = maxObject.transform.localScale.y;
        SetMeterColor();
    }

    public void ChangeMeter(float drunkValue)
    {
        meter += drunkValue;

        SetMeter();
        SetMeterColor();
    }

    private void SetMeterColor()
    {
        Color drunkColor = Color.Lerp(baseColor, this.drunkColor, meter / 100f);
        meterObject.GetComponent<SpriteRenderer>().color = drunkColor;
        textObject.GetComponent<TextMesh>().color = drunkColor;
    }

    private void SetMeter()
    {
        Vector3 meterLength = meterObject.transform.localScale;
        meterLength.y = (maxLength / 100) * meter;
        meterLength.y = Mathf.Clamp(meterLength.y, 0f, maxLength);

        if (meterLength.y >= maxLength)
            isFull = true;
        else
            isFull = false;

        meterObject.transform.localScale = meterLength;
    }

    public void Reset()
    {
        meter = 0;
        SetMeter();
    }

    public void LessDrunk()
    {
        meter /= 2f;
        SetMeter();
    }
}