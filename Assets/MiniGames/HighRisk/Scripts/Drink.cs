﻿using UnityEngine;

public class Drink : MonoBehaviour
{

    public GameObject fluidObject;
    public TextMesh riskText;

    private HighRiskManager manager;
    private drinkData drinkValues;
    private Color waterColor;

    private void Start()
    {
        manager = FindObjectOfType<HighRiskManager>();
    }

    private void Update()
    {
        if (!manager.inGame) { return; }

        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector3.forward);

        if (hit.collider == null) { return; }

        if (hit.collider.gameObject == gameObject && Input.GetMouseButtonDown(0))
        {
            manager.DrinkDrunk(hit.collider.gameObject);
        }
    }

    public void SetupDrink(Color[] fluidColors)
    {
        waterColor = fluidColors[0];
        this.drinkValues = MakeRandomValues(fluidColors);

        SetFluid();
        SetRisk();
    }

    public drinkData GetDrinkData()
    {
        return drinkValues;
    }

    private void SetFluid()
    {
        float fluidAmount;

        fluidObject.GetComponent<SpriteRenderer>().color = drinkValues.fluidColor;

        switch (gameObject.name)
        {
            case "Beer":
                fluidAmount = Random.Range(1.5f, 3f);
                break;
            case "Shot":
                fluidAmount = Random.Range(0.45f, .9f);
                break;
            case "Wine":
                fluidAmount = Random.Range(0.75f, 1.5f);
                break;
            case "Glass":
                fluidAmount = Random.Range(1.5f, 3f);
                break;
            default:
                fluidAmount = 0;
                break;
        }
        Vector3 fluidScale = new Vector3(4f, fluidAmount, 1f);
        fluidObject.transform.localScale = fluidScale;
    }

    private void SetRisk()
    {
        riskText.text = drinkValues.alcahol.ToString() + "%";
    }

    private drinkData MakeRandomValues(Color[] fluidColors)
    {
        drinkData drinkValues = new drinkData();

        int randomFluidColorIndex = Random.Range(0, fluidColors.Length);
        drinkValues.fluidColor = fluidColors[randomFluidColorIndex];

        if (drinkValues.fluidColor == waterColor)
        {
            drinkValues.alcahol = 0;
        }
        else
        {
            drinkValues = CalculateRisk(drinkValues);
        }
        drinkValues.amountOfFluid = Mathf.RoundToInt(Random.Range(25, 95));
        return drinkValues;
    }

    private static drinkData CalculateRisk(drinkData drinkValues)
    {
        drinkValues.alcahol = Mathf.RoundToInt(Random.Range(3, 45));
        return drinkValues;
    }
}

public struct drinkData
{
    public int amountOfFluid;
    public int alcahol;

    public Color fluidColor;
}
