﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ChangeState : MonoBehaviour
{
    private States state;

    private void Awake()
    {
        state = (States)Enum.Parse(typeof(States), gameObject.name);

        string sceneToLoad = "";
        if (GetComponentInChildren<Text>())
        {
            Text text = GetComponentInChildren<Text>();

            if (text.gameObject.name != "ignore")
                sceneToLoad = text.text;
        }

        GetComponent<Button>().onClick.AddListener(() => FSM.Instance.InvokeState(state, sceneToLoad));
    }
}
