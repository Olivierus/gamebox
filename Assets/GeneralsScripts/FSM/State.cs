﻿using UnityEngine;

public abstract class State : MonoBehaviour
{
    public bool done { get; set; }

    public virtual void OnEnter() { }
    public virtual void Tick() { }
    public virtual void OnExit() { done = true; }
}
