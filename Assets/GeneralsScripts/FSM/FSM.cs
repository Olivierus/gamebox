﻿using System;
using System.Collections;
using UnityEngine;

public class FSM : MonoBehaviour
{
    private State state;

    private static FSM _instance;

    public static FSM Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject go = new GameObject("FSM", typeof(FSM));
            }
            return _instance;
        }
    }

    private void Awake()
    {
        _instance = this;
        DontDestroyOnLoad(gameObject);
        state = gameObject.AddComponent<LoadLogin>();
        // FMODAudioManager.gameLoaderBackingTrack.start();
        state.OnEnter();
    }

    private void Update()
    {
        state.Tick();
    }

    public State StatesEnumToState(States state)
    {
        Type stateType = Type.GetType(state.ToString());
        State nextState = (State)gameObject.AddComponent(stateType);
        return nextState;
    }

    public void InvokeState(States state, string sceneToLoad = "")
    {
        State stateToChangeTo = FSM.Instance.StatesEnumToState(state);

        try
        {
            if (sceneToLoad != "")
            {
                SceneLoader sl = (SceneLoader)stateToChangeTo;
                sl.levelName = sceneToLoad;

                if (sl.levelName == "Login")
                {
                    sl.levelName = "Home";
                }

                stateToChangeTo = sl;
            }
        }
        catch { Debug.LogWarning("No sceneloader"); }

        FSM.Instance.SwitchStates(stateToChangeTo);
    }

    public void SwitchStates(State nextState)
    {
        StartCoroutine(HandleSwitch(nextState));
    }

    IEnumerator HandleSwitch(State nextState)
    {
        Debug.Log(nextState);

        if (state == null) { yield return null; }

        state.OnExit();

        while (!state.done)
            yield return null;

        Destroy(state);

        state = nextState;

        state.OnEnter();

        yield return null;
    }
}
