﻿using System;
using UnityEngine;

public class GameTutorial : State
{
    public override void OnEnter()
    {
        string str = "UI_" + PlayerPrefs.GetString("currentGame") + "_Tutorial";
        UI ui = (UI)Enum.Parse(typeof(UI), str);
        UIManager.Instance.ShowUIPanel(ui);
    }

    public override void OnExit()
    {
        UIManager.Instance.DestroyCurrentUIPanel();
        done = true;
    }
}
