﻿using System;
using UnityEngine;

public class PlayGame : State, IAnimationCallback
{
    public override void OnEnter()
    {
        try
        {
            SetAnimationEvent(FindObjectOfType<Animator>());
        }
        catch
        {
            GameObject.Find("GameManager").GetInterface<IMiniGameManager>().StartGame();
            done = true;
        }
    }

    private void SetAnimationEvent(Animator anim)
    {
        AnimationClip clip = anim.runtimeAnimatorController.animationClips[0];
        anim.gameObject.AddComponent<AnimationEventTrigger>();
        Utilities.SetupAnimationEvent(anim, 0, clip.length, this);
        anim.SetTrigger("PlayGame");
    }

    public void HandleCallback()
    {
        GameObject.Find("GameManager").GetInterface<IMiniGameManager>().StartGame();
        done = true;
    }
}
