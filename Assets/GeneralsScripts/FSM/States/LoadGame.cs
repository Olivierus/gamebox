﻿public class LoadGame : SceneLoader
{
    public override void Tick()
    {
        if (isSceneLoaded)
        {
            FSM.Instance.SwitchStates(FSM.Instance.StatesEnumToState(States.GameMenu));
            isSceneLoaded = false;
        }
    }
}
