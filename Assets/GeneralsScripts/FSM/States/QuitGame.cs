﻿
using UnityEngine;

public class QuitGame : SceneLoader
{
    public override void OnEnter()
    {
        Application.Quit();
    }
}
