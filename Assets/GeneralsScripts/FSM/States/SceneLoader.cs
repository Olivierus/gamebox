﻿using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine;

public class SceneLoader : State
{
    private AsyncOperation async;
    public string levelName;
    public bool isSceneLoaded = false;

    public override void OnEnter()
    {
        if (SceneManager.GetActiveScene().name != levelName)
        {
            StartCoroutine(ShowSceneWhenLoaded(levelName));
        }
        else
        {
            isSceneLoaded = true;
            Debug.LogWarning(levelName + " allready loaded");
        }

        PlayerPrefs.SetString("currentGame", levelName);
    }

    IEnumerator ShowSceneWhenLoaded(string levelName)
    {
        AsyncOperation async = SceneManager.LoadSceneAsync(levelName);
        async.allowSceneActivation = false;
        FMODAudioManager.globalSoundSceneLoaded.oneShot();
        while (async.progress < .9f)
        {
            Debug.LogWarning("loading : " + levelName);
        }
        async.allowSceneActivation = true;
        yield return new WaitForSeconds(0.1f);
        isSceneLoaded = true;
        done = true;
        yield return null;
    }
}
