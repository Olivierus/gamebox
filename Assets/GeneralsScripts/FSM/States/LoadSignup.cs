﻿using UnityEngine;

public class LoadSignup : State
{
    public override void OnEnter()
    {
        UIManager.Instance.DestroyCurrentUIPanel();
        UIManager.Instance.ShowUIPanel(UI.UI_Signup);
    }

    public override void OnExit()
    {
        done = true;
    }
}
