﻿using UnityEngine;
using UnityEngine.UI;

public class Login : State
{
    private string password;
    private string username;

    private QueryHandler<loginData> qh;

    public override void OnEnter()
    {
        password = GameObject.Find("Password").transform.GetComponent<InputField>().text;
        username = GameObject.Find("Username").transform.GetComponent<InputField>().text;

        string[] queryArguments = {"Username="+username,
                                    "Password="+password};

        qh = new QueryHandler<loginData>("signin", queryArguments);

        StartCoroutine(qh.HandleServerRequest());
    }

    public override void Tick()
    {
        loginData result;
        qh.RequestDone(out result);
        if (!done)
            HandleResult(result, GameObject.Find("Error").GetComponent<Text>());
    }

    private void HandleResult(loginData result, Text output)
    {
        PlayerPrefs.SetString("session_id", result.SSID);

        if (result.ErrorCode == "-1")
        {
        FSM.Instance.InvokeState(States.LoadHome, "Login");
        done = true;
        }
        else
        { output.text = result.ErrorCode; }
    }
}

[System.Serializable]
public struct loginData
{
    public string SSID;
    public string ErrorCode;
}
