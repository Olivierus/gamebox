﻿using UnityEngine;

public class LoadLogin : State
{
    public override void OnEnter()
    {
        UIManager.Instance.ShowUIPanel(UI.UI_Login);
    }

    public override void OnExit()
    {
        done = true;
    }
}
