﻿using System.Collections;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class SignUp : State
{
    private string username;
    private string password;
    private string email;
    private string birthDate;
    private QueryHandler<loginData> qh;

    public override void OnEnter()
    {
        password = GameObject.Find("Password").transform.GetComponent<InputField>().text;
        username = GameObject.Find("Username").transform.GetComponent<InputField>().text;
        email = GameObject.Find("Email").transform.GetComponent<InputField>().text;
        birthDate = GameObject.Find("Birth date").transform.GetComponent<InputField>().text;

        string[] queryArguments = {"Username="+username,
                                    "Password="+password,
                                    "Email="+email,
                                    "Dateofbirth="+birthDate};

        qh = new QueryHandler<loginData>("signup", queryArguments);

        StartCoroutine(qh.HandleServerRequest());
    }

    public override void Tick()
    {
        loginData result;
        qh.RequestDone(out result);

        HandleResult(result, GameObject.Find("Error").GetComponent<Text>());
    }

    private void HandleResult(loginData result, Text output)
    {
        if (result.ErrorCode == "-1")
        {
            FSM.Instance.InvokeState(States.LoadLogin, "");
            done = true;
        }
        else
        {
            output.text = result.ErrorCode;
        }
    }
}
