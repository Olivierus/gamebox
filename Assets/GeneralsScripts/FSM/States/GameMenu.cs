﻿using System;
using System.Collections;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class GameMenu : State
{
    private UI activeSceneUI;
    private string activeSceneName;

    private QueryHandler<loginData> qh;

    public override void OnEnter()
    {
        activeSceneName = PlayerPrefs.GetString("currentGame");

        activeSceneUI = CheckIfGameMangerExist(activeSceneName);

        UIManager.Instance.ShowUIPanel(activeSceneUI);

        ServerRequest();
    }

    public override void Tick()
    {
        loginData log;
        qh.RequestDone(out log);
    }

    public override void OnExit()
    {
        UIManager.Instance.DestroyCurrentUIPanel();
        done = true;
    }

    private void ServerRequest()
    {
        string[] queryArguments = { "ActiveGame=" + activeSceneName };

        qh = new QueryHandler<loginData>("changeactivegame", queryArguments);

        StartCoroutine(qh.HandleServerRequest());
    }

    private UI CheckIfGameMangerExist(string sceneName)
    {
        if (!GameObject.Find("GameManager"))
        {
            Type gameManagerType = Type.GetType(sceneName + "Manager");
            GameObject go = new GameObject("GameManager", gameManagerType);
        }
        return (UI)Enum.Parse(typeof(UI), "UI_" + sceneName);
    }
}
