﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause : State
{
    public override void OnEnter()
    {
    }

    public override void Tick()
    {
    }

    public override void OnExit()
    {
        done = true;
    }
}
