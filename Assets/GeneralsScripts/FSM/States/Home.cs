﻿public class Home : State
{
    public override void OnEnter()
    {
        UIManager.Instance.ShowUIPanel(UI.UI_Home);
        FindObjectOfType<GenerateGameButtons>().CreateButtons();
    }

    public override void OnExit()
    {
        done = true;
    }
}
