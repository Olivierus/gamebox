﻿using System;
using UnityEngine;

public class GameOver : GetScores, IAnimationCallback
{

    private QueryHandler<scoreData> queryHandler;

    public override void OnEnter()
    {
        GameObject.Find("GameManager").GetInterface<IMiniGameManager>().GameOver();

        UIManager.Instance.ShowUIPanel(UI.UI_GameOver);

        SetCurrentScore();

        base.OnEnter();
    }

    public override void Tick()
    {
        base.Tick();
    }

    public override void OnExit()
    {
        try
        {
            SetAnimationEvent(FindObjectOfType<Animator>());
            return;
        }
        catch
        {
            IMiniGameManager miniGameManager = GameObject.Find("GameManager").GetInterface<IMiniGameManager>();
            miniGameManager.RestartGame();
            done = true;
        }
        UIManager.Instance.DestroyCurrentUIPanel();
        done = true;
    }

    public void HandleCallback()
    {
        GameObject.Find("GameManager").GetInterface<IMiniGameManager>().RestartGame();
        done = true;
    }

    private void SetAnimationEvent(Animator anim)
    {
        anim.gameObject.AddComponent<AnimationEventTrigger>();
        Utilities.SetupAnimationEvent(anim, 1, 0, this);
        anim.SetTrigger("GameOver");
    }

    private void SetCurrentScore()
    {
        GameObject gameManagerObject = GameObject.Find("GameManager");
        IMiniGameManager miniGameManagerComponent = gameManagerObject.GetInterface<IMiniGameManager>();
        string[] queryArguments = { "Score=" + Mathf.Round(miniGameManagerComponent.score).ToString() };

        queryHandler = new QueryHandler<scoreData>("setscore", queryArguments);
        StartCoroutine(queryHandler.HandleServerRequest());
    }
}
