﻿using UnityEngine;

public class GameScore : GetScores
{
    public override void OnEnter()
    {
        UIManager.Instance.ShowUIPanel(UI.UI_Score);
        base.OnEnter();
    }

    public override void OnExit()
    {
        UIManager.Instance.DestroyCurrentUIPanel();
        done = true;
    }
}
