﻿public class LoadHome : SceneLoader
{
    public override void Tick()
    {
        if (isSceneLoaded)
        {
            FSM.Instance.SwitchStates(FSM.Instance.StatesEnumToState(States.Home));
            isSceneLoaded = false;
        }
    }
}
