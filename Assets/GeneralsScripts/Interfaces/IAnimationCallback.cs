﻿public interface IAnimationCallback
{
    void HandleCallback();
}
