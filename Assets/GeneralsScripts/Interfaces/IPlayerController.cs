﻿public interface IPlayerController
{
    PlayerControllerData data { get; set; }
    bool grounded { get; set; }
    void Move();
    void Jump();
}
