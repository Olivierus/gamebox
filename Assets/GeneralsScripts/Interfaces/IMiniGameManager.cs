﻿public interface IMiniGameManager
{
    bool inGame { get; set; }
    float score { get; set; }
    string unit { get; }

    void StartGame();
    void RestartGame();
    void GameOver();
}
