using UnityEngine;

namespace GlobalSoundAM{
  public class SceneLoaded : BaseAudioManager
  {
    public SceneLoaded() : base("event:/GlobalSound/SceneLoaded"){

    }
  }
}
