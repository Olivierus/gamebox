using UnityEngine;

namespace GlobalSoundAM{
  public class ButtonClickSound : BaseAudioManager
  {
    public ButtonClickSound() : base("event:/GlobalSound/ButtonClicked"){

    }
  }
}
