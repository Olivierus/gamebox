using UnityEngine;

public class BaseAudioManager
{
  // --------------------------------- Vars --------------------------------- //
  protected string eventName;
  protected FMOD.Studio.EventInstance eventInstance;

  // ----------------------------- Constructor ------------------------------ //
  public BaseAudioManager(string eventName)
  {
    this.eventName = eventName;
    eventInstance = FMODUnity.RuntimeManager.CreateInstance(eventName);
  }

  // -------------------------- Default Functions --------------------------- //
  public void start()
  {
    eventInstance.start();
  }
  public void stop()
  {
    eventInstance.stop (FMOD.Studio.STOP_MODE.IMMEDIATE);
  }
  public void oneShot()
  {
    FMODUnity.RuntimeManager.PlayOneShot(eventName);
  }
  protected void setParam(FMOD.Studio.ParameterInstance param, float value)
  {
    param.setValue(value);
  }
}
