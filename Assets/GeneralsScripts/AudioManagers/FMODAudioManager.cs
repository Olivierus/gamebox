﻿using UnityEngine;

public static class FMODAudioManager
{
  // ----------------------------- GlobalSound ------------------------------ //
  public static GlobalSoundAM.SceneLoaded  globalSoundSceneLoaded  = new GlobalSoundAM.SceneLoaded();
  public static GlobalSoundAM.ButtonClickSound gameLoaderButtonClicked = new GlobalSoundAM.ButtonClickSound();

  // --------------------------- GameLoaderScreen --------------------------- //
  public static GameLoaderScreenAM.BackingTrack gameLoaderBackingTrack = new GameLoaderScreenAM.BackingTrack();

  // ----------------------------- WashingTime ------------------------------ //
  public static WashingTimeAM.HighScoreSound WashingTimeUIHighscore = new WashingTimeAM.HighScoreSound();
  public static WashingTimeAM.HowToSound     WashingTimeUIHowTo = new WashingTimeAM.HowToSound();
  public static WashingTimeAM.StartGameSound WashingTimeUIStartGame = new WashingTimeAM.StartGameSound();


}
