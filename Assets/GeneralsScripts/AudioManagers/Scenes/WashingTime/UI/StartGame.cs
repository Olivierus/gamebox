using UnityEngine;

namespace WashingTimeAM{
  public class StartGameSound : BaseAudioManager
  {
    public StartGameSound() : base("event:/Scene/WashingTime/UI/StartGame"){

    }
  }
}
