using UnityEngine;

namespace WashingTimeAM{
  public class HighScoreSound : BaseAudioManager
  {
    public HighScoreSound() : base("event:/Scene/WashingTime/UI/HighScore"){

    }
  }
}
