using UnityEngine;

namespace GameLoaderScreenAM{
  public class BackingTrack : BaseAudioManager
  {
    public FMOD.Studio.ParameterInstance MiniGameParamEvent;
    public BackingTrack() : base("event:/Scene/GameLoaderScreen/BackingTrack"){

    }

    public void miniGameLoaded(){
      eventInstance.getParameter("Scene Loaded", out MiniGameParamEvent);
      this.setParam(MiniGameParamEvent, 1.0f);
    }
  }
}
