public enum UI{
UI_ActionReaction,
UI_ActionReaction_Tutorial,
UI_BalancingAtWork,
UI_BalancingAtWork_Tutorial,
UI_GameOver,
UI_HighRisk,
UI_HighRisk_Tutorial,
UI_Home,
UI_Login,
UI_Pause,
UI_Score,
UI_Signup,
UI_WashingTime,
UI_WashingTime_Tutorial
}
