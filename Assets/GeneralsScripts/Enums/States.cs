public enum States{
GameMenu,
GameOver,
GameScore,
GameTutorial,
GetScores,
Home,
LoadGame,
LoadHome,
LoadLogin,
LoadSignup,
Login,
Pause,
PlayGame,
QuitGame,
SceneLoader,
SignUp
}
