﻿using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class Timer : MonoBehaviour
{
    private bool startTimer = false;
    private bool countdown;
    private Text timerText;
    private float startTime;
    private float time;
    private System.Action countdownEvent;

    private void Start()
    {
        timerText = GetComponent<Text>();
        string startTimeText = timerText.text;
        startTimeText = Regex.Replace(startTimeText, "[^0-9.]", "");
        startTime = time = float.Parse(startTimeText);
    }

    private void FixedUpdate()
    {
        if (!startTimer) { return; }

        if (countdown)
        {
            time -= Time.fixedDeltaTime;

            if (time <= 0)
            {
                if (countdownEvent != default)
                {
                    startTimer = false;
                    countdownEvent.Invoke();
                }
                else
                {
                    countdown = false;
                }
            }
        }
        else
        {
            time += Time.fixedDeltaTime;
        }

        timerText.text = Mathf.RoundToInt(time).ToString() + "s";
    }

    public void StartTimer(bool countdown = false, System.Action countdownEvent = default)
    {
        ResetTimer();
        this.countdownEvent = countdownEvent;
        this.countdown = countdown;
        startTimer = true;
    }

    public int GetTime()
    {
        return Mathf.RoundToInt(time);
    }

    public void StopTimer()
    {
        startTimer = false;
    }

    public void ResetTimer()
    {
        time = startTime;
    }
}
