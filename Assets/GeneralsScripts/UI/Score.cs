﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public Text rank;
    public Text username;
    public Text score;

    public void SetScore(string rank, string username, string score)
    {
        this.rank.text = rank;
        this.username.text = username;
        this.score.text = score;
    }
}
