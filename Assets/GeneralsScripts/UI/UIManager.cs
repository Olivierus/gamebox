﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using System;

public class UIManager : MonoBehaviour
{
    private GameObject[] UIPanels;

    private List<UIPanel> currentUIPanels = new List<UIPanel>();
    private UIPanel currentUIPanel;
    private UIPanel lastUIPanel;

    private static UIManager _instance;

    public static UIManager Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject go = new GameObject("UIManager", typeof(UIManager));
            }
            return _instance;
        }
    }

    private void Awake()
    {
        _instance = this;
        DontDestroyOnLoad(gameObject);
        UIPanels = Resources.LoadAll("General/UI", typeof(GameObject)).Cast<GameObject>().ToArray();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ToggleUIPanel(UI.UI_Pause);
        }
    }

    public void ShowUIPanel(UI UIPanelName, bool permanent = false)
    {
        if (!UIPanels.Any(obj => obj.name == UIPanelName.ToString()))
        {
            Debug.LogError("No UI panel with that name");
            return;
        }

        if (CheckIfUIPanelExist(UIPanelName)) { return; }

        DestroyCurrentUIPanel();

        currentUIPanel = CreateUIPanel(UIPanelName, permanent);
    }

    private bool CheckIfUIPanelExist(UI UIPanelName)
    {
        foreach (UIPanel uiPanel in currentUIPanels)
        {
            if (uiPanel.panel.name == UIPanelName.ToString())
                return true;
        }

        return false;
    }

    public void ToggleUIPanel(UI UIPanelName)
    {
        if (!currentUIPanel.panel)
        {
            ShowUIPanel(UIPanelName, false);
            lastUIPanel = new UIPanel();
            return;
        }

        if (currentUIPanel.panel && !lastUIPanel.panel)
        {
            lastUIPanel = currentUIPanel;
            lastUIPanel.panel.SetActive(false);
            currentUIPanel = CreateUIPanel(UIPanelName, false);
        }
        else
        {
            lastUIPanel.panel.SetActive(true);
            DestroyCurrentUIPanel();
            currentUIPanel = lastUIPanel;
            lastUIPanel = default;
        }
    }

    public void DestroyCurrentUIPanel()
    {
        if (currentUIPanel.permanent) { return; }

        try
        {
            currentUIPanels.Remove(currentUIPanel);
            Destroy(currentUIPanel.panel);
        }
        catch
        {
            Destroy(currentUIPanel.panel);
        }

    }

    private UIPanel CreateUIPanel(UI UIPanelName, bool permanent)
    {
        GameObject go = Instantiate(UIPanels.Where(obj => obj.name == UIPanelName.ToString()).SingleOrDefault());
        go.name = UIPanelName.ToString();

        UIPanel newUIPanel = new UIPanel();
        newUIPanel.panel = go;
        newUIPanel.permanent = permanent;
        return newUIPanel;
    }
}


public struct UIPanel
{
    public GameObject panel;
    public bool permanent;
}