﻿using UnityEngine;
using UnityEngine.UI;

public class StartAnimation : MonoBehaviour
{
    public string animationToPlay;
    private Animator anim;
    private AnimationClip clip;

    private void Start()
    {
        anim = Camera.main.GetComponent<Animator>();
        GetComponent<Button>().onClick.AddListener(StartAnimationAnimator);
    }

    private void StartAnimationAnimator()
    {
        anim.SetTrigger(animationToPlay);
    }
}
