﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class ShowScore : MonoBehaviour
{
    private void Start()
    {
        IMiniGameManager miniGameManager = GameObject.Find("GameManager").GetInterface<IMiniGameManager>();
        GetComponent<Text>().text = "Your score was: " + Mathf.Round(miniGameManager.score).ToString() + " " + miniGameManager.unit;
    }
}
