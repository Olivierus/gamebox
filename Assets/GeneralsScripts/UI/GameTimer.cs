﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Text))]
public class GameTimer : MonoBehaviour
{
    public float time;
    public bool gameStarted = false;

    private bool isCountingDown = false;
    private Text timeText;
    private IMiniGameManager miniGameManager;
    private System.Action createGameElements;

    private void Awake()
    {
        timeText = GetComponent<Text>();
        time = System.Int32.Parse(timeText.text);
    }

    private void FixedUpdate()
    {
        if (!gameStarted) { return; }

        GetGameManager();

        if (isCountingDown)
        {
            time += Time.fixedDeltaTime;
            timeText.text = Mathf.Round(time).ToString();
            miniGameManager.score = this.time;
        }
        else if (!isCountingDown)
        {
            time -= Time.fixedDeltaTime;
            timeText.text = Mathf.Round(time).ToString();
            miniGameManager.score = this.time;

            if (time <= 0)
            {
                isCountingDown = true;
                createGameElements.Invoke();
            }
        }
    }

    private void GetGameManager()
    {
        if (miniGameManager != null) { return; }

        miniGameManager = GameObject.Find("GameManager").GetInterface<IMiniGameManager>();
    }

    public void CountDown(System.Action createObstacles)
    {
        isCountingDown = false;
        timeText.text = "3";
        time = 3;

        createGameElements = createObstacles;
        gameStarted = true;
    }
}
