﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class Utilities
{
    public static Vector2[] CreateCircle(int numberOfEdges, float radius)
    {
        Vector2[] points = new Vector2[numberOfEdges + 1];

        for (int i = 0; i < numberOfEdges; i++)
        {
            float angle = 2 * Mathf.PI * i / numberOfEdges;
            float x = radius * Mathf.Cos(angle);
            float y = radius * Mathf.Sin(angle);

            points[i] = new Vector2(x, y);
        }
        points[numberOfEdges] = points[0];
        return points;
    }

    public static string GetFileName(string path)
    {
        int indexPrePath = 0;
        for (int i = 0; i < path.Length; i++)
        {
            if (path[i] == '/')
            {
                indexPrePath = i;
            }
            else if (path[i] == '\u005c')
            {
                indexPrePath = i;
            }
        }

        string fileName = path.Remove(0, indexPrePath + 1);
        if (fileName.IndexOf('.') != -1)
        {
            fileName = fileName.Remove(fileName.IndexOf('.'));
        }
        return fileName;
    }

    public static Vector3 RandomVector3(float min, float max)
    {
        float x = UnityEngine.Random.Range(min, max);
        float y = UnityEngine.Random.Range(min, max);
        float z = UnityEngine.Random.Range(min, max);
        Vector3 randomVector = new Vector3(x, y, z);
        return randomVector;
    }

    public static Vector3 RandomVector3(Rect boundry)
    {
        float minX = boundry.x - boundry.width / 2;
        float maxX = boundry.x + boundry.width / 2;
        float x = UnityEngine.Random.Range(minX, maxX);

        float minY = boundry.y - boundry.height / 2;
        float maxY = boundry.y + boundry.height / 2;
        float y = UnityEngine.Random.Range(minY, maxY);

        Vector3 randomVector = new Vector3(x, y, 0);
        return randomVector;
    }

    public static void SetupAnimationEvent(Animator anim, int animationIndex, float time, State sender)
    {
        AnimationClip clip;
        clip = anim.runtimeAnimatorController.animationClips[animationIndex];

        clip.events = new AnimationEvent[0];

        AnimationEvent evt;
        evt = new AnimationEvent();
        evt.time = time;
        evt.functionName = "Trigger";
        evt.objectReferenceParameter = sender;
        clip.AddEvent(evt);
    }

    public static Vector3 GetMouseWorldPosition()
    {
        Vector3 mouseToWorldPosition = Camera.main.ScreenToWorldPoint(UnityEngine.Input.mousePosition);
        mouseToWorldPosition.z = 0f;
        return mouseToWorldPosition;
    }
}
