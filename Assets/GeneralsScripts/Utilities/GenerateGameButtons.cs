﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GenerateGameButtons : MonoBehaviour
{
    private CreateGrid grid;

    public void CreateButtons()
    {
        GameObject[] buttons = new GameObject[SceneManager.sceneCountInBuildSettings - 2];

        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i] = CreateGameButtons(i);
        }

        grid = gameObject.AddComponent<CreateGrid>();

        Rect canvasRect = GetComponent<RectTransform>().rect;
        canvasRect.width = canvasRect.width * transform.localScale.x;
        canvasRect.height = canvasRect.height * transform.localScale.y;
        buttons = grid.GenerateGridWorldSpace(buttons, 2, canvasRect);
    }

    private GameObject CreateGameButtons(int i)
    {
        GameObject button;

        string gameScenePath = SceneUtility.GetScenePathByBuildIndex(i + 2);
        string gameName = Utilities.GetFileName(gameScenePath);

        button = Instantiate(Resources.Load<GameObject>("LoadGame"), transform.position, Quaternion.identity, transform);
        button.name = "LoadGame";
        button.GetComponentInChildren<Text>().text = gameName;
        button.GetComponent<Image>().sprite = Resources.Load<Sprite>("General/GameButtons/" + gameName);
        button.AddComponent<ChangeState>();

        return button;
    }
}
