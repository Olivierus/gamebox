﻿using System;
using UnityEngine;

public class CreateGrid : MonoBehaviour
{
    public GameObject[] GenerateGridWorldSpace(GameObject gridObject, int totalGridObjects, int gridObjectsPerRow, Rect space)
    {
        GameObject[] gos = new GameObject[totalGridObjects * gridObjectsPerRow];

        for (int x = 0; x < gridObjectsPerRow; x++)
        {
            int t = (totalGridObjects / gridObjectsPerRow);
            for (int y = 0; y < t; y++)
            {
                float xObject = (space.width / gridObjectsPerRow) * x + ((space.width / gridObjectsPerRow) / 2);
                float yObject = (space.height / t) * y + ((space.height / t) / 2);

                Vector3 screenPositionObject = new Vector3(xObject, yObject, Camera.main.nearClipPlane);
                GameObject go = Instantiate(gridObject, screenPositionObject, Quaternion.identity, transform);
                go.name = gridObject.name;
                gos[t * x + y] = go;
            }
        }
        return gos;
    }

    public GameObject[] GenerateGridWorldSpace(GameObject[] gridObject, int gridObjectsPerRow, Rect space)
    {
        int totalGridObjects = gridObject.Length;
        GameObject[] gos = new GameObject[totalGridObjects * gridObjectsPerRow];

        for (int x = 0; x < gridObjectsPerRow; x++)
        {
            if ((totalGridObjects / gridObjectsPerRow) <= 0) { gridObjectsPerRow = totalGridObjects; }

            int t = (totalGridObjects / gridObjectsPerRow);

            for (int y = 0; y < t; y++)
            {
                int index = t * x + y;
                float xObject = (space.width / gridObjectsPerRow) * x + ((space.width / gridObjectsPerRow) / 2);
                float yObject = (space.height / t) * y + ((space.height / t) / 2);

                Vector3 screenPositionObject = new Vector3(xObject, yObject, Camera.main.nearClipPlane);
                gridObject[index].transform.position = screenPositionObject;
                gos[index] = gridObject[index];
            }
        }
        return gos;
    }

    public GameObject[] GenerateGridScreenSpace(GameObject gridObject, int totalGridObjects, int gridObjectsPerRow, Rect space)
    {
        GameObject[] gos = new GameObject[totalGridObjects * gridObjectsPerRow];

        for (int x = 0; x < gridObjectsPerRow; x++)
        {
            int t = Mathf.RoundToInt(totalGridObjects / gridObjectsPerRow);
            for (int y = 0; y < t; y++)
            {
                float xObject = (space.width / gridObjectsPerRow) * x + ((space.width / gridObjectsPerRow) / 2);
                float yObject = (space.height / t) * y + ((space.height / t) / 2);

                Vector3 screenPositionObject = new Vector3(xObject, yObject, Camera.main.nearClipPlane);
                Vector3 nextObjectPosition = Camera.main.ScreenToWorldPoint(screenPositionObject);
                GameObject go = Instantiate(gridObject, nextObjectPosition, Quaternion.identity, transform);
                go.name = gridObject.name;
                gos[t * x + y] = go;
            }
        }
        return gos;
    }

    public GameObject[] GenerateGridScreenSpace(GameObject[] gridObject, int gridObjectsPerRow, Rect space)
    {
        int totalGridObjects = gridObject.Length;
        GameObject[] gos = new GameObject[totalGridObjects * gridObjectsPerRow];

        for (int x = 0; x < gridObjectsPerRow; x++)
        {
            if ((totalGridObjects / gridObjectsPerRow) <= 0) { gridObjectsPerRow = totalGridObjects; }

            int t = (totalGridObjects / gridObjectsPerRow);

            for (int y = 0; y < t; y++)
            {
                int index = t * x + y;
                float xObject = (space.width / gridObjectsPerRow) * x + ((space.width / gridObjectsPerRow) / 2) + space.x;
                float yObject = (space.height / t) * y + ((space.height / t) / 2) + space.y;

                Vector3 screenPositionObject = new Vector3(xObject, yObject, Camera.main.nearClipPlane);
                Vector3 nextObjectPosition = Camera.main.ScreenToWorldPoint(screenPositionObject);
                gridObject[index].transform.position = nextObjectPosition;
                gos[index] = gridObject[index];
            }
        }
        return gos;
    }
}
