﻿using System;
using UnityEngine;

public class AnimationEventTrigger : MonoBehaviour
{
    public void Trigger(State handler)
    {
        handler.GetComponent<IAnimationCallback>().HandleCallback();
        Destroy(this);
    }
}
