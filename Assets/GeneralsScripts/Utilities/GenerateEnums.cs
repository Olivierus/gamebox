﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System;
using System.Linq;

#if (UNITY_EDITOR) 

[InitializeOnLoad]
public class GenerateEnums
{
    static GenerateEnums()
    {
        EditorApplication.projectChanged += OnScriptsReloaded;
    }

    private static string output = "Assets/GeneralsScripts/Enums/";
    private static void OnScriptsReloaded()
    {
        CreateEnum("Assets/GeneralsScripts/FSM/States", "*.cs*.meta");
        CreateEnum("Assets/Resources/General/UI", "*.prefab*.meta");
    }

    private static void CreateEnum(string path, string filterExtensions)
    {
        string fileName = output + Utilities.GetFileName(path) + ".cs";
        StreamWriter sr;

        if (File.Exists(fileName))
        {
            sr = new StreamWriter(fileName);
        }
        else
        {
            sr = File.CreateText(fileName);
        }

        sr.WriteLine("public enum " + Utilities.GetFileName(path) + '{');
        FillEnum(sr, path, filterExtensions);
        sr.WriteLine('}');
        sr.Close();
    }

    private static void FillEnum(StreamWriter sr, string path, string filterExtensions)
    {
        string[] filePaths = Directory.GetFiles(path, filterExtensions);

        for (int i = 0; i < filePaths.Length; i++)
        {
            string enumEntryName = Utilities.GetFileName(filePaths[i]);
            enumEntryName = enumEntryName.Replace(" ", "");

            if (i != filePaths.Length - 1)
            {
                enumEntryName += ",";
            }

            sr.WriteLine(enumEntryName);
        }
    }
}
#endif